//
//  main.m
//  LX ProductScan
//
//  Created by Jeff Kase on 3/3/20.
//  Copyright © 2020 Jeff Kase. All rights reserved.
//

@import UIKit;
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    NSString * appDelegateClassName;
    @autoreleasepool {
        // Setup code that might create autoreleased objects goes here.
        appDelegateClassName = NSStringFromClass([AppDelegate class]);
    }
    return UIApplicationMain(argc, argv, nil, appDelegateClassName);
}
