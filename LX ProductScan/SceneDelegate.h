//
//  SceneDelegate.h
//  LX ProductScan
//
//  Created by Jeff Kase on 3/3/20.
//  Copyright © 2020 Jeff Kase. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

