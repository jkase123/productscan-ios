//
//  AppDelegate+NSURLSession.m
//  Locator
//
//  Created by Jeff Kase on 4/4/16.
//  Copyright © 2016 LocatorX. All rights reserved.
//

#import "AppDelegate+UrlSession.h"

@implementation AppDelegate (UrlSession)

#ifdef STAGING
    #define kCSMToken  @"ba0dad7d-532c-4282-826c-1d6449aa3ab3"
    #define kConsumerAuthToken  @"c3fe3999-5640-4957-9134-e96ddf47f020"
#else
    #ifdef DEV
        #define kCSMToken  @"ba0dad7d-532c-4282-826c-1d6449aa3ab3"
        #define kConsumerAuthToken  @"c3fe3999-5640-4957-9134-e96ddf47f020"
    #else
        #define kCSMToken  @"7e38cb50-ea1a-4493-8190-5e1c59e702b6"
        #define kConsumerAuthToken  @"825dfcf6-5869-4b70-980f-102e252fe423"
    #endif
#endif

-(void)initializeSession
{
//    self.hasStartup = NO;
    
    NSString *language = [[NSLocale preferredLanguages] objectAtIndex:0];
    NSLog(@"current language: %@", language);
    
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    [sessionConfiguration setHTTPAdditionalHeaders:@{ @"Accept" : @"application/json" }];
    
    self.urlSession = [NSURLSession sessionWithConfiguration:sessionConfiguration delegate:nil delegateQueue:nil];
    
    [self appSettings:kAppName withBlock:nil];
}

-(void)appSettings:(NSString *)appName withBlock:(void(^)(NSMutableDictionary *json)) block
{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@appSettings/mobile/%@", kAPIURLBase, appName]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request addValue:kCSMToken forHTTPHeaderField:@"CSM"];
    
    [[self.urlSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (!data) {
            if (block)
                block([@{@"error":@"no data"} mutableCopy]);
        } else {
            NSMutableDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];

            self.privacyPolicy = json[@"privacyPolicy"];
            if (!self.privacyPolicy || self.privacyPolicy.length == 0)
                self.privacyPolicy = kPrivacyPolicy;
            
            self.termsOfUse = json[@"termsOfUse"];
            if (!self.termsOfUse || self.termsOfUse.length == 0)
                self.termsOfUse = kTermsOfUse;

            [[NSNotificationCenter defaultCenter] postNotificationName:kAppSettingsChange object:nil];

//            if (block)
//                block(json);
        }
    }] resume];
}

-(void)setRootViewController:(UIViewController *)viewController
{
    if ([NSThread isMainThread]) {
        self.window.rootViewController = viewController;
    } else {
        [self performSelectorOnMainThread:@selector(setRootViewController:) withObject:viewController waitUntilDone:NO];
    }
}

#ifdef NotNow
-(NSString *)findAssetIdFromQRUrl:(NSString *)url
{
    NSArray<NSString *> *parts = [url componentsSeparatedByString:@"/"];
    for (NSString *part in parts) {
        //        NSLog(@"part: %@", part);
        if (part.length == 36) {
            return part;
        }
    }
    NSArray *urlComponents = [url componentsSeparatedByString:@"?"];
    if (urlComponents.count == 2) {
        urlComponents = [urlComponents[1] componentsSeparatedByString:@"&"];
        for (NSString *keyPair in urlComponents) {
            NSArray *tagComponents = [keyPair componentsSeparatedByString:@"="];
            if ([tagComponents[0] isEqualToString:@"assetId"]) {
                return tagComponents[1];
            }
        }
    }

    return nil;
}
#endif

-(void)postAssetScan:(NSDictionary *)parameters withBlock:(void (^)(NSMutableDictionary *))block
{
    NSURL *url = nil;
    if (parameters[@"assetId"])
        url = [NSURL URLWithString:[NSString stringWithFormat:@"%@assets/csm/mobile/%@/scanFromApp", kAPIURLBase, parameters[@"assetId"]]];
    else
        url = [NSURL URLWithString:[NSString stringWithFormat:@"%@assets/csm/mobile/scanFromAppNFC", kAPIURLBase]];

    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    NSLog(@"getAssetScan: %@", url.absoluteString);
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:kConsumerAuthToken forHTTPHeaderField:@"auth-token"];
    [request addValue:kCSMToken forHTTPHeaderField:@"CSM"];

    [request setHTTPMethod:@"POST"];
    NSError *error;
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:&error];
    [request setHTTPBody:postData];

    [[self.urlSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (data) {
//            NSString *test = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            NSMutableDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            NSLog(@"getAssetScan: %@", json);
            block(json);
        }
    }] resume];
}

#ifdef NotNow
-(void)getSmartLabelsWithBlock:(void (^)(NSMutableDictionary *))block
{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/GetSmartLabels", kAPIURLBase]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [[self.urlSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (data) {
            NSMutableDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            NSLog(@"smartLabel: %@", json);
            block(json);
        }
    }] resume];
}

-(void)getDecryptedString:(NSDictionary *)parameters withUUID:(NSString *)uuid withBlock:(void(^)(NSMutableDictionary *json)) block
{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/GetDecryptedString?uuid=%@", kAPIURLBase, uuid]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [request setHTTPMethod:@"POST"];
    NSError *error;
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:&error];
    [request setHTTPBody:postData];
    
    [[self.urlSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (!data) {
            if (block)
                block([@{@"error":@"no data"} mutableCopy]);
        } else {
            NSMutableDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            NSLog(@"%@", json);
            if (json[@"success"]) {
            }
            if (block)
                block(json);
        }
        
    }] resume];
}
#endif

@end
