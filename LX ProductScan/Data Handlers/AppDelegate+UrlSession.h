//
//  AppDelegate+NSURLSession.h
//  Locator
//
//  Created by Jeff Kase on 4/4/16.
//  Copyright © 2016 LocatorX. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate (UrlSession)

-(void)initializeSession;

-(void)setRootViewController:(UIViewController *)viewController;
//-(NSString *)findAssetIdFromQRUrl:(NSString *)url;

-(void)postAssetScan:(NSDictionary *)parameters withBlock:(void (^)(NSMutableDictionary *))block;

@end
