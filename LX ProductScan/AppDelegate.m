//
//  AppDelegate.m
//  ProductScan
//
//  Created by Jeff Kase on 6/8/19.
//  Copyright © 2019 Jeff Kase. All rights reserved.
//

#import "AppDelegate.h"
#import "AppDelegate+Location.h"
#import "AppDelegate+UrlSession.h"
#import "AppDelegate+Data.h"
#import "UIColor+Convert.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [self initializeSession];
    [self initializeLocation];
    [self initializeData];

    self.buttonColor = [UIColor colorFromHexString:@"3D5BA9"];
    self.button2Color = [UIColor colorFromHexString:@"FFC854"];
    self.text1Color = [UIColor colorFromHexString:@"5884F5"];
    self.text2Color = [UIColor colorFromHexString:@"36562F"];
    self.text3Color = [UIColor colorFromHexString:@"BF83EA"];

    [[UINavigationBar appearance] setBarTintColor:self.text2Color];
    [[UINavigationBar appearance] setTranslucent:NO];
    
    UIView *statusBar = [[UIView alloc]initWithFrame:[UIApplication sharedApplication].keyWindow.windowScene.statusBarManager.statusBarFrame] ;
    statusBar.backgroundColor = [UIColor whiteColor];
    [[UIApplication sharedApplication].keyWindow addSubview:statusBar];
    
    return YES;
}

- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void (^)(NSArray<id<UIUserActivityRestoring>> * _Nullable))restorationHandler
{
    NSLog(@"userActivity: %@: %@", userActivity.activityType, userActivity.webpageURL);
    NSString *urlString = userActivity.webpageURL.absoluteString;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kAssetIdReceived object:nil userInfo:@{ @"urlString":urlString }];

    return true;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
