//
//  AppDelegate.h
//  ProductScan
//
//  Created by Jeff Kase on 6/8/19.
//  Copyright © 2019 Jeff Kase. All rights reserved.
//

@import UIKit;
@import CoreLocation;

//#define kAPIURLBase     @"http://192.168.1.248:8080/lx-atlas/api/"

#define kAppSettingsChange  @"appSettingsChange"
#define kAssetIdReceived    @"assetIdReceived"

#ifdef STAGING
    #define kAPIURLBase     @"https://api-staging.locatorx.com/lx-atlas/api/"
#else
    #if DEV
        #define kAPIURLBase     @"https://api-dev.locatorx.com/lx-atlas/api/"
    #else
        #define kAPIURLBase     @"https://api.locatorx.com/lx-atlas/api/"
    #endif
#endif

#define kAppName        @"LXProductScan"

#define kFAQPageUrl @"https://lxproductscan.locatorx.com/faq"
#define kPrivacyPolicy  @"https://wiki.locatorx.com/privacy-policy"
#define kTermsOfUse     @"https://wiki.locatorx.com/terms-of-use"
#define kLocatorX       @"https://www.locatorx.com"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UIColor *buttonColor, *button2Color, *text1Color, *text2Color, *text3Color;

// AppDelegate+NSURLSession
//
@property (nonatomic) NSURLSession *urlSession;
@property NSString *privacyPolicy, *termsOfUse, *connectHome;

// AppDelegate+Data
//
@property (nonatomic) NSMutableDictionary *m_data;

- (NSURL *)applicationDocumentsDirectory;

// AppDelegate+Location
//
@property (nonatomic) CLLocationManager *locationManager;
@property (nonatomic) CLLocation *currentLocation;
@property bool locationServicesAuthorized, locationActive, inBackground;
@property NSTimeInterval lastLocationReportTime;
@property (copy) void(^locationBlock)(CLLocation *location);

@end

