//
//  ScanViewController.m
//  LX ProductScan
//
//  Created by Jeff Kase on 3/10/20.
//  Copyright © 2020 Jeff Kase. All rights reserved.
//

@import AudioToolbox;

#import "ScanViewController.h"
#import "QRScannerViewController.h"
#import "UIViewController+AppDelegate.h"
#import "AppDelegate+Location.h"
#import "AppDelegate+UrlSession.h"
#import "AppDelegate+Data.h"
#import "NSMutableArray+Assets.h"
#import "NSMutableDictionary+Asset.h"
#import "NSCombined+Values.h"
#import "UIColor+Convert.h"
#import "UIViewController+Alert.h"
#import "AlertViewController.h"
#import "Lockbox.h"
#import "ConnectViewController.h"

@interface ScanViewController () <QRScannerResult>

@property (nonatomic) NSMutableDictionary *json;
@property (nonatomic) NSArray *catalogues;
@property (weak, nonatomic) IBOutlet UINavigationItem *profileButton;
@property (weak, nonatomic) IBOutlet UILabel *topLabel;
@property (weak, nonatomic) IBOutlet UILabel *subTitle;
@property (weak, nonatomic) IBOutlet UIButton *scanButton;

@end

@implementation ScanViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    NSString *result = [Lockbox stringForKey:@"sound"];
    if (!result)
        [Lockbox setString:@"true" forKey:@"sound"];
    
    result = [Lockbox stringForKey:@"vibrate"];
    if (!result)
        [Lockbox setString:@"true" forKey:@"vibrate"];

    result = [Lockbox stringForKey:@"history"];
    if (!result)
        [Lockbox setString:@"true" forKey:@"history"];

    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlack];
    if (!self.navigationItem.rightBarButtonItem.image)
        [self.navigationItem.rightBarButtonItem setImage:[UIImage imageNamed:@"icon_profile"]];
    //    if (@available(iOS 13, *)) {
    //        NSLog(@"got 13");
    //    } else {
    //        NSLog(@"prior to 13");
    //        [self.navigationItem.rightBarButtonItem setImage:[UIImage imageNamed:@"icon_profile"]];
    //    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage: [UIImage imageNamed: @"LocatorX"]];

    self.scanButton.backgroundColor = self.appDelegate.buttonColor;
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(scannerClosed:) name:kScannerClosed object:nil];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)found:(NSString *)result withSourceTag:(NSInteger)tag viewController:(UIViewController *)viewController andBlock:(void(^)(bool rescan)) block
{
    NSLog(@"found result: %@", result);
    
    if (![self setAssetIdFromResult:result]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self alertWithTitle:@"Error"
                      andMessage:@"Invalid QR Code"
                    cancelButton:@"OK"
                      andButtons:nil
                   andTextFields:nil
                        andBlock:^(int iCount, NSArray *fields){
                NSLog(@"block true from found");
                            block(true);
                        }
             ];
        });
    } else {
        [self fetchAssetScanInfoWithBlock:block];
    }
}

-(void)fetchAssetScanInfoWithBlock:(void(^)(bool rescan)) block
{
    NSString *sound = [Lockbox stringForKey:@"sound"];
    if ([@"true" isEqualToString:sound]) {
        SystemSoundID soundId;
        NSString *path = [[NSBundle mainBundle] pathForResource:@"Chime" ofType:@"wav"];
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)[NSURL fileURLWithPath:path], &soundId);
        AudioServicesPlaySystemSoundWithCompletion(soundId, ^{
            NSLog(@"play sound");
            AudioServicesDisposeSystemSoundID(soundId);
        });
    }
    
    NSString *vibrate = [Lockbox stringForKey:@"vibrate"];
    if ([@"true" isEqualToString:vibrate])
        AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);

    NSLog(@"fetchAssetScanInfo %@ %@", self.assetId, @"null");
    
    NSMutableDictionary *postBody = [@{
        @"latitude":@(self.appDelegate.currentLocation.coordinate.latitude),
        @"longitude":@(self.appDelegate.currentLocation.coordinate.longitude)
    } mutableCopy];
    if (self.assetId)
        postBody[@"assetId"] = self.assetId;
    if (self.qrHash)
        postBody[@"qrHash"] = self.qrHash;
    
    [self.appDelegate postAssetScan:postBody withBlock:^(NSMutableDictionary *json) {
        NSLog(@"**** asset scan result");
        dispatch_sync(dispatch_get_main_queue(), ^{
            self.json = json;
            if (json[@"asset"]) {
                NSString *url = json[@"url"];
                NSString *result = [Lockbox stringForKey:@"history"];
                if (url != nil && result && [result isEqualToString:@"true"])
                    [self addAssetToList:self.assetId withUrl:url andTag:json[@"asset"][@"tag"]];
                    
                NSLog(@"block false from post");
                block(false);
                [self performSegueWithIdentifier:@"ConnectView" sender:self];
            } else {
                [self alertWithTitle:@"Error"
                          andMessage:@"Invalid QR Code"
                        cancelButton:@"OK"
                          andButtons:nil
                       andTextFields:nil
                            andBlock:^(int iCount, NSArray *fields){
                    NSLog(@"block true from post");
                                block(true);
                            }
                ];
            }
        });
    }];
}

 //#define kMaxAssets 6
 #define kMaxAssets 25

-(void)addAssetToList:(NSString *)assetId withUrl:(NSString *)url andTag:(NSString *)tag
 {
 //    NSLog(@"assets: %@", [self.appDelegate.assets getAssetsByTime]);
     NSMutableDictionary *assetRecord = [self.appDelegate.assets getAssetForId:assetId];
     if (!assetRecord) {
         assetRecord = [self.appDelegate.assets newAsset];
         assetRecord.assetId = assetId;
     }
     NSDate *now = [NSDate date];
     assetRecord.timeOfVisit = (int)now.timeIntervalSince1970;
     
     NSDateFormatter *dfOut = [[NSDateFormatter alloc] init];
     [dfOut setDateFormat:[NSDateFormatter dateFormatFromTemplate:@"MMddyyyyhhmmssa" options:0 locale:NSLocale.systemLocale]];

//     NSInteger timeZoneSeconds = [[NSTimeZone localTimeZone] secondsFromGMT];
//     NSDate *gmtDate = [NSDate dateWithTimeIntervalSince1970:assetRecord.timeOfVisit];
//     NSDate *localDate = [gmtDate dateByAddingTimeInterval:timeZoneSeconds];
     assetRecord.timeOfVisitString = [dfOut stringFromDate:now];
     assetRecord.url = url;
     assetRecord.tag = tag;
     
     if (self.appDelegate.assets.count > kMaxAssets) {   // this must be done after the timeOfVisit is set, so the new one in doesn't get trimmed!
         NSMutableArray *sortedAssets = [self.appDelegate.assets getAssetsByTime];
         self.appDelegate.assets = [[sortedAssets subarrayWithRange:NSMakeRange(0, kMaxAssets)] mutableCopy];
     }

     [self.appDelegate saveAssetData];
 //    NSLog(@"assets: %@", [self.appDelegate.assets getAssetsByTime]);
 }

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([@"QRScan" isEqualToString:segue.identifier]) {
        QRScannerViewController *qrvc = segue.destinationViewController;
        qrvc.handler = self;
        qrvc.sourceTag = -1;
    } else if ([@"ConnectView" isEqualToString:segue.identifier]) {
        ConnectViewController *cvc = segue.destinationViewController;
        cvc.json = self.json;
    } else {
        [super prepareForSegue:segue sender:sender];
    }
}

@end
