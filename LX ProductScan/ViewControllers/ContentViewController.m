//
//  ContentViewController.m
//  LX ProductScan
//
//  Created by Jeff Kase on 3/10/20.
//  Copyright © 2020 Jeff Kase. All rights reserved.
//

#import "ContentViewController.h"
#import "UIViewController+AppDelegate.h"
#import "AppDelegate+UrlSession.h"
#import "WebViewController.h"

@interface ContentViewController ()

@end

@implementation ContentViewController

-(UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller
{
    return UIModalPresentationNone;
}

-(void)doFAQ
{
    [self performSegueWithIdentifier:@"FAQ" sender:self];
}

-(void)doPrivacyPolicy
{
    [self performSegueWithIdentifier:@"Privacy" sender:self];
}

-(void)doTerms
{
    [self performSegueWithIdentifier:@"Terms" sender:self];
}

-(void)doLocatorX
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:kLocatorX] options:@{} completionHandler:nil];
//    [self performSegueWithIdentifier:@"LocatorX" sender:self];
}

-(void)doEventHistory
{
    [self performSegueWithIdentifier:@"EventHistory" sender:self];
}

-(bool)setAssetIdFromResult:(NSString *)url
{
    self.assetId = nil;
    self.qrHash = nil;
    
    NSArray *result = [ContentViewController parseAssetIdHashFromResult:url];
    switch (result.count) {
        case 2:
            self.qrHash = result[1];
            // fall through
        case 1:
            self.assetId = result[0];
            return true;
    }
    return false;
}

+(NSArray *)parseAssetIdHashFromResult:(NSString *)url
{
    NSURL *resultURL = [NSURL URLWithString:url];
    NSLog(@"url: %@", resultURL.query);
    NSString *assetId = nil;
    NSString *hash = nil;
    
    if (resultURL.query && resultURL.query.length > 5) {
        NSArray *urlComponents = [resultURL.query componentsSeparatedByString:@"&"];
        for (NSString *keyPair in urlComponents) {
            NSArray *tagComponents = [keyPair componentsSeparatedByString:@"="];
            if ([tagComponents[0] isEqualToString:@"assetId"]) {
                assetId = tagComponents[1];
            } else if ([tagComponents[0] isEqualToString:@"hash"]) {
                hash = tagComponents[1];
            }
        }
    }
    if (!assetId) {
        for (NSString *path in resultURL.pathComponents) {
            NSLog(@"  path: %@", path);
            if ([[NSUUID alloc] initWithUUIDString:path]) {
                assetId = path;
            }
        }
    }

    NSLog(@"assetId: %@ hash: %@", assetId, hash);
    if (assetId && hash)
        return @[assetId, hash];
    
    if (assetId)
        return @[assetId];
    
    return @[];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([@"ProfilePopover" isEqualToString:segue.identifier]) {
        ProfileViewController *pfvc = segue.destinationViewController;
        pfvc.preferredContentSize = CGSizeMake(300, 420);
        pfvc.popoverPresentationController.delegate = self;
        pfvc.popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionUp;
        pfvc.popoverPresentationController.barButtonItem = sender;
        
        pfvc.handler = self;
    } else if ([@"FAQ" isEqualToString:segue.identifier]) {
        WebViewController *wvc = segue.destinationViewController;
        wvc.url = kFAQPageUrl;
        wvc.webTitle = @"FAQ";
    } else if ([@"Privacy" isEqualToString:segue.identifier]) {
        WebViewController *wvc = segue.destinationViewController;
        wvc.url = kPrivacyPolicy;
        wvc.webTitle = @"Privacy Policy";
    } else if ([@"Terms" isEqualToString:segue.identifier]) {
        WebViewController *wvc = segue.destinationViewController;
        wvc.url = kTermsOfUse;
        wvc.webTitle = @"Terms & Conditions";
    } else if ([@"LocatorX" isEqualToString:segue.identifier]) {
        WebViewController *wvc = segue.destinationViewController;
        wvc.url = kLocatorX;
        wvc.webTitle = @"LocatorX";
    } else if ([@"EventHistory" isEqualToString:segue.identifier]) {
    }
}

@end
