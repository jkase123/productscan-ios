//
//  ViewController.m
//  ProductScan
//
//  Created by Jeff Kase on 6/8/19.
//  Copyright © 2019 Jeff Kase. All rights reserved.
//

@import CoreNFC;
@import WebKit;
@import AudioToolbox;

#import "ViewController.h"
#import "QRScannerViewController.h"
#import "UIViewController+AppDelegate.h"
#import "AppDelegate+Location.h"
#import "AppDelegate+UrlSession.h"
#import "AppDelegate+Data.h"
#import "NSMutableArray+Assets.h"
#import "NSMutableDictionary+Asset.h"
#import "NSCombined+Values.h"
#import "SimpleListViewController.h"
#import "UIColor+Convert.h"
#import "UIViewController+Alert.h"
#import "AlertViewController.h"
#import "HintViewController.h"
#import "Lockbox.h"

@interface ViewController () <NFCNDEFReaderSessionDelegate, QRScannerResult, WKNavigationDelegate, WKUIDelegate, UIPopoverPresentationControllerDelegate, SimpleListHandler, UITabBarDelegate, HintChange>

@property (weak, nonatomic) WKWebView *webView;
@property (weak, nonatomic) IBOutlet WKWebView *scanWebView;
@property (weak, nonatomic) IBOutlet WKWebView *historyWebView;
@property (weak, nonatomic) IBOutlet WKWebView *settingsWebView;

@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *hintButton;
@property (weak, nonatomic) IBOutlet UIView *securityBackground;
@property (weak, nonatomic) IBOutlet UIImageView *securityImage;
@property (weak, nonatomic) IBOutlet UILabel *securityText;

@property (nonatomic) NFCNDEFReaderSession *nfcSession;
@property (nonatomic) NSString *qrResult;
@property (nonatomic) NSString *nfcResult, *assetId, *url, *scanMode;
@property (nonatomic) NSArray *scanModes;
@property (weak, nonatomic) IBOutlet UITabBar *tabBar;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.backButton.enabled = NO;
#ifdef NotNow
    self.forwardButton.enabled = NO;
    self.affinities = @[
        @"Manufacturer",
        @"Logistics",
        @"Retailer",
        @"Consumer"
    ];
    self.affinity = @"Consumer";
    self.scanModes = @[
        @"Both",
        @"QR Code Only",
        @"NFC Only"
    ];
#endif
    self.scanMode = @"QR Code Only";
    
    self.scanWebView.navigationDelegate = self;
    self.scanWebView.UIDelegate = self;
    self.historyWebView.navigationDelegate = self;
    self.historyWebView.UIDelegate = self;
    self.settingsWebView.navigationDelegate = self;
    self.settingsWebView.UIDelegate = self;
    
    self.webView = self.scanWebView;

    [self.tabBar setSelectedItem:[self.tabBar.items objectAtIndex:0]];
    self.tabBar.delegate = self;
    
    self.securityBackground.hidden = YES;
    [self.securityBackground setAlpha:0.0f];
    
    NSString *result = [Lockbox stringForKey:@"sound"];
    if (!result)
        [Lockbox setString:@"true" forKey:@"sound"];
    
    result = [Lockbox stringForKey:@"vibrate"];
    if (!result)
        [Lockbox setString:@"true" forKey:@"vibrate"];
    
    result = [Lockbox stringForKey:@"history"];
    if (!result)
        [Lockbox setString:@"true" forKey:@"history"];

    NSSet *websiteDataTypes = [NSSet setWithArray:@[
                                                    WKWebsiteDataTypeDiskCache,
                                                    WKWebsiteDataTypeOfflineWebApplicationCache,
                                                    WKWebsiteDataTypeMemoryCache,
                                                    //WKWebsiteDataTypeLocalStorage,
                                                    WKWebsiteDataTypeCookies,
                                                    //WKWebsiteDataTypeSessionStorage,
                                                    //WKWebsiteDataTypeIndexedDBDatabases,
                                                    //WKWebsiteDataTypeWebSQLDatabases,
                                                    WKWebsiteDataTypeFetchCache, //(iOS 11.3, *)
                                                    //WKWebsiteDataTypeServiceWorkerRegistrations, //(iOS 11.3, *)
                                                    ]];
    NSDate *dateFrom = [NSDate dateWithTimeIntervalSince1970:0];
    [[WKWebsiteDataStore defaultDataStore] removeDataOfTypes:websiteDataTypes modifiedSince:dateFrom completionHandler:^{
    }];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(settingsChanged:) name:kAppSettingsChange object:nil];

    [self.scanWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.appDelegate.connectHome]]];
    [self.historyWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:kHistoryPageUrl]]];
    [self.settingsWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:kSettingsPageUrl]]];
}

-(void)settingsChanged:(NSNotification *)note
{
    dispatch_sync(dispatch_get_main_queue(), ^{
        [self.scanWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.appDelegate.connectHome]]];
    });
}

-(void)clearBrowserHistory:(WKWebView *)webView
{
    if (webView.canGoBack) {
        WKBackForwardList *bfList = webView.backForwardList;
        [webView goToBackForwardListItem:[bfList itemAtIndex:0]];
    }
}

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    switch (item.tag) {
        case 0:
//            [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.appDelegate.connectHome]]];
            self.webView = self.scanWebView;
            self.scanWebView.hidden = NO;
            self.historyWebView.hidden = YES;
            self.settingsWebView.hidden = YES;
            break;
        case 1:
//            [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:kHistoryPageUrl]]];
            self.webView = self.historyWebView;
            self.scanWebView.hidden = YES;
            self.historyWebView.hidden = NO;
            self.settingsWebView.hidden = YES;
            if (!self.webView.canGoBack)
                [self.webView reload];
            break;
        case 2:
//            [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:kSettingsPageUrl]]];
            self.webView = self.settingsWebView;
            self.scanWebView.hidden = YES;
            self.historyWebView.hidden = YES;
            self.settingsWebView.hidden = NO;
            break;
    }
//    [self clearBrowserHistory:self.webView];
    self.backButton.enabled = self.webView.canGoBack;
}

- (IBAction)scanPressed:(id)sender
{
    NSLog(@"start scanning");
    if ([self.scanMode isEqualToString:@"NFC Only"]) {
        [self startNFCScan];
    } else {
        [self performSegueWithIdentifier:@"QRScanner" sender:self];
//        [self presentViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"QRScanner"] animated:YES completion:nil];
    }
}

- (IBAction)backPressed:(id)sender
{
    [self.webView goBack];
    if (!self.webView.canGoBack)
        [self.webView reload];
}

-(void)found:(NSString *)result
{
    NSLog(@"found result: %@", result);
    self.qrResult = result;
    
    if ([self.scanMode isEqualToString:@"Both"])
        [self startNFCScan];
    else
        [self processAssetScan];
}

-(void)startNFCScan
{
    if (!self.nfcSession) {
        self.nfcResult = nil;
        
        self.nfcSession = [[NFCNDEFReaderSession alloc] initWithDelegate:self queue:nil invalidateAfterFirstRead:NO];
        [self.nfcSession beginSession];
    }
}

- (void)webView:(WKWebView *)webView runJavaScriptTextInputPanelWithPrompt:(NSString *)prompt defaultText:(NSString *)defaultText initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(NSString *result))completionHandler
{
    NSString *result = @"done";
    if ([prompt hasPrefix:@"scan"]) {
        [self scanPressed:self];
    } else if ([prompt hasPrefix:@"logout"] || [prompt hasPrefix:@"home"]) {
        [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.appDelegate.connectHome]]];
    } else if ([prompt hasPrefix:@"settings"]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString] options:@{} completionHandler:nil];
    } else if ([prompt hasPrefix:@"scrollEnable"]) {
        webView.scrollView.scrollEnabled = YES;
    } else if ([prompt hasPrefix:@"scrollDisable"]) {
        webView.scrollView.scrollEnabled = NO;
    } else if ([prompt hasPrefix:@"loadAssetHistory"]) {
        result = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:[self.appDelegate.assets getAssetsByTime] options:0 error:nil] encoding:NSUTF8StringEncoding];
    } else if ([prompt hasPrefix:@"saveAssetHistory="]) {
        NSArray *items = [prompt componentsSeparatedByString:@"="];
        if (items.count == 2) {
            NSMutableArray *ahArray = [NSJSONSerialization JSONObjectWithData:[items[1] dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:nil];
            self.appDelegate.assets = ahArray;
            [self.appDelegate saveAssetData];
        }
    } else if ([prompt hasPrefix:@"openUrl "]) {
        NSArray *items = [prompt componentsSeparatedByString:@" "];
        if (items.count == 2) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:items[1]] options:@{} completionHandler:nil];
        }
#ifdef NotNow
    } else if ([prompt hasPrefix:@"tokenExpired"]) {
        [self alertWithTitle:@"Error"
                  andMessage:@"Your session has expired"
                cancelButton:@"OK"
                  andButtons:nil
               andTextFields:nil
                    andBlock:^(int index, NSArray *textFields) {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [self.appDelegate logout];
                                });
        }];
#endif
    } else if ([prompt hasPrefix:@"get="]) {
        NSArray *items = [prompt componentsSeparatedByString:@"="];
        if (items.count == 2) {
            result = [Lockbox stringForKey:items[1]];
        }
    } else if ([prompt hasPrefix:@"set="]) {
        NSArray *items = [prompt componentsSeparatedByString:@"="];
        if (items.count == 3) {
            result = [Lockbox stringForKey:items[2]];
            [Lockbox setString:items[2] forKey:items[1]];
        }
    } else if ([prompt hasPrefix:@"toggle="]) {
        NSArray *items = [prompt componentsSeparatedByString:@"="];
        if (items.count == 2) {
            result = [Lockbox stringForKey:items[1]];
            if (!result || [result isEqualToString:@"false"]) {
                result = @"true";
                [Lockbox setString:@"true" forKey:items[1]];
            } else {
                result = @"false";
                [Lockbox setString:@"false" forKey:items[1]];
            }
        }
    }
    completionHandler(result);
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation
{
    if (webView == self.webView)
        self.backButton.enabled = webView.canGoBack;
//    self.forwardButton.enabled = webView.canGoForward;
}

- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error
{
    NSLog(@"***** failed nav *****");
}

/*
- (void)readerSession:(NFCReaderSession *)session didDetectTags:(NSArray<__kindof id<NFCTag>> *)tags
{
    for (NFCTag tag : tags) {
        
    }
}
 */

- (void)readerSession:(nonnull NFCNDEFReaderSession *)session didDetectNDEFs:(nonnull NSArray<NFCNDEFMessage *> *)messages
{
    NSLog(@"didDetect");
    NSLog(@"  %i messages", (int)messages.count);
    for (NFCNDEFMessage *message in messages) {
        for (NFCNDEFPayload *record in message.records) {
            unsigned char buffer[2000];
            memset(buffer, 0, 2000);

#ifdef NotNow
 //           [record.identifier getBytes:buffer length:200];
            [record.payload getBytes:buffer length:200];
 //           [record.type getBytes:buffer length:200];
            switch (record.typeNameFormat) {
                case NFCTypeNameFormatNFCWellKnown:
                    NSLog(@"Well Known");
                    self.nfcResult = [NSString stringWithCString:(const char *)(buffer + 3) encoding:NSUTF8StringEncoding];
                    break;
                case NFCTypeNameFormatMedia:
                    NSLog(@"Media");
                    break;
                case NFCTypeNameFormatAbsoluteURI:
                    NSLog(@"URI");
                    break;
                case NFCTypeNameFormatNFCExternal:
                    NSLog(@"External");
                    break;
                case NFCTypeNameFormatUnknown:
                    NSLog(@"Unknown");
                    break;
                case NFCTypeNameFormatUnchanged:
                    NSLog(@"Unchanged");
                    break;
                case NFCTypeNameFormatEmpty:
                    NSLog(@"Empty");
                    break;
                default:
                    NSLog(@"Invalid Data");
                    break;
            }
#else
            [record.payload getBytes:buffer length:200];
            if (record.typeNameFormat == NFCTypeNameFormatNFCWellKnown) {
                self.nfcResult = [NSString stringWithCString:(const char *)(buffer + 3) encoding:NSUTF8StringEncoding];
//                [self processAssetScan];
            }
#endif
        }
    }
    [self.nfcSession invalidateSession];
    self.nfcSession = nil;
}

- (void)readerSession:(nonnull NFCNDEFReaderSession *)session didInvalidateWithError:(nonnull NSError *)error
{
    NSLog(@"didInvalidateWithError: %@", error.localizedDescription);
    self.nfcSession = nil;
    
    dispatch_sync(dispatch_get_main_queue(), ^{
        [self processAssetScan];
    });
}

-(void)processAssetScan
{
    NSArray<NSString *> *parts = [self.qrResult componentsSeparatedByString:@"/"];
    for (NSString *part in parts) {
        //        NSLog(@"part: %@", part);
        if (part.length == 36) {
            self.assetId = part;
            break;
        }
    }

    [self fetchAssetScanInfo];
}

-(void)fetchAssetScanInfo
{
    NSLog(@"fetchAssetScanInfo %@ %@", self.assetId, (self.nfcResult ? self.nfcResult : @"null"));
    
    NSMutableDictionary *postBody = [@{
        @"latitude":@(self.appDelegate.currentLocation.coordinate.latitude),
        @"longitude":@(self.appDelegate.currentLocation.coordinate.longitude)
    } mutableCopy];
    if (self.assetId)
        postBody[@"assetId"] = self.assetId;
    if (self.nfcResult)
        postBody[@"nfcString"] = self.nfcResult;
    
    [self.appDelegate getAssetScan:postBody withBlock:^(NSMutableDictionary *json) {
        NSLog(@"**** asset scan result");
        dispatch_sync(dispatch_get_main_queue(), ^{
            NSMutableDictionary *assetMap = json[@"asset"];
            //                self.assetLabel.text = assetMap[@"tag"];
            
            bool valid = [self showSecurityElements:[json[@"isCounterfeit"] safeBoolValue]
                                          missingQR:[json[@"missingQR"] safeBoolValue]
                                         missingNFC:[json[@"missingNFC"] safeBoolValue]
                                           nfcValid:[json[@"nfcValid"] safeBoolValue]];
            if (!self.assetId)
                self.assetId = assetMap[@"assetId"];
            
            if (self.assetId) {
                NSString *urlFromServer = json[@"url"];
                bool containsParams = [urlFromServer containsString:@"?"];
                NSString *validString = [NSString stringWithFormat:@"%@valid=%@", (containsParams ? @"&" : @"?"), (valid ? @"true" : @"false")];
                self.url = [NSString stringWithFormat:@"%@%@", urlFromServer, validString];
                NSString *result = [Lockbox stringForKey:@"history"];
                if (self.url != nil && result && [result isEqualToString:@"true"]) {
                    [self addAssetToList:self.assetId];
                }
            }
            NSLog(@"Opening: %@", self.url);
            [self.scanWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.url]]];
        });
    }];
}

//#define kMaxAssets 6
#define kMaxAssets 25

-(void)addAssetToList:(NSString *)assetId
{
    NSLog(@"assets: %@", [self.appDelegate.assets getAssetsByTime]);
    NSMutableDictionary *assetRecord = [self.appDelegate.assets getAssetForId:assetId];
    if (!assetRecord) {
        assetRecord = [self.appDelegate.assets newAsset];
        assetRecord.assetId = assetId;
    }
    NSDate *now = [NSDate date];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
    assetRecord.timeOfVisit = (int)now.timeIntervalSince1970;
    assetRecord.timeOfVisitString = [df stringFromDate:now];
    assetRecord.url = self.url;
    
    if (self.appDelegate.assets.count > kMaxAssets) {   // this must be done after the timeOfVisit is set, so the new one in doesn't get trimmed!
        NSMutableArray *sortedAssets = [self.appDelegate.assets getAssetsByTime];
        self.appDelegate.assets = [[sortedAssets subarrayWithRange:NSMakeRange(0, kMaxAssets)] mutableCopy];
    }

    [self.appDelegate saveAssetData];
    NSLog(@"assets: %@", [self.appDelegate.assets getAssetsByTime]);
}

-(void)showSecurityBannerOK:(bool)ok withHidden:(bool)hidden fade:(bool)fade
{
    self.securityBackground.hidden = YES;
    [self.securityBackground setAlpha:0.0f];
    if (hidden)
        return;
    
    self.securityBackground.hidden = NO;
    if (ok) {
        self.securityBackground.backgroundColor = [UIColor colorFromHexString:@"a0c46d"];
        self.securityText.text = @"Valid";
        self.securityImage.image = [UIImage imageNamed:@"white_check"];
    } else {
        self.securityBackground.backgroundColor = [UIColor colorFromHexString:@"eb4f35"];
        self.securityText.text = @"WARNING";
        self.securityImage.image = [UIImage imageNamed:@"white_x"];
    }
    if (fade) {
        [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
            // fade in
            [self.securityBackground setAlpha:1.0f];
        } completion:^(BOOL finished) {
            //fade out
            [UIView animateWithDuration:1.0 delay:2.5 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
                [self.securityBackground setAlpha:0.0f];
            } completion:nil];
        }];
    } else {
        [self.securityBackground setAlpha:1.0f];
    }
}

-(bool)showSecurityElements:(BOOL)isCounterfeit missingQR:(BOOL)missingQR missingNFC:(BOOL)missingNFC nfcValid:(BOOL)nfcValid
{
    BOOL showAlert = NO;
    UIColor *alertBackgroundColor;
    NSString *alertText;
    bool valid = YES;
    
    [self showSecurityBannerOK:YES withHidden:NO fade:YES];

    if (isCounterfeit) {
//        showAlert = YES;
//        alertBackgroundColor = [UIColor colorFromHexString:@"ca670f"];
//        alertText = @"This product may be counterfeit";
//        [self.lockImage setImage:[UIImage imageNamed:@"orange_lock"]];
        [self showSecurityBannerOK:NO withHidden:NO fade:YES];
        valid = NO;
    } else {
        if (nfcValid) {
            if (missingQR && ![self.scanMode isEqualToString:@"NFC Only"]) {
//                showAlert = YES;
//                alertBackgroundColor = [UIColor colorFromHexString:@"2348d2"];
//                alertText = @"No QR Code was found";
//            [self.lockImage setImage:[UIImage imageNamed:@"blue_lock"]];
                [self showSecurityBannerOK:NO withHidden:NO fade:YES];
                valid = NO;
            } else {
//            [self.lockImage setImage:[UIImage imageNamed:@"green_lock"]];
            }
        }
        if (missingNFC && ![self.scanMode isEqualToString:@"QR Code Only"]) {
//            showAlert = YES;
//            alertText = @"No NFC chip was found";
//        [self.lockImage setImage:[UIImage imageNamed:@"blue_lock"]];
            [self showSecurityBannerOK:NO withHidden:NO fade:YES];
            valid = NO;
        }
    }
    if (!missingNFC && !nfcValid) {
//        showAlert = YES;
//        alertBackgroundColor = [UIColor colorFromHexString:@"ca0f0f"];
//        alertText = @"This product is likely counterfeit";
//        [self.lockImage setImage:[UIImage imageNamed:@"red_lock"]];
        [self showSecurityBannerOK:NO withHidden:NO fade:YES];
        valid = NO;
    }
    if (showAlert) {
        AlertViewController *avc = [self.storyboard instantiateViewControllerWithIdentifier:@"AlertPopup"];
        avc.modalPresentationStyle = UIModalPresentationPopover;
        avc.preferredContentSize = CGSizeMake(300, 80);
        avc.view.backgroundColor = alertBackgroundColor;
        avc.popoverPresentationController.delegate = self;
        avc.popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionUp;
#ifdef NotNow
        avc.popoverPresentationController.sourceView = self.lockImage;
        avc.popoverPresentationController.sourceRect = self.lockImage.bounds;
#else
        avc.popoverPresentationController.sourceView = self.backButton;
        avc.popoverPresentationController.sourceRect = self.backButton.bounds;
#endif
        avc.label.text = alertText;
        
        [self presentViewController:avc animated:YES completion:nil];
        
        AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
    }
    return valid;
}

#pragma mark - SimpleList

#ifdef NotNow
-(void)simpleListDidSelect:(NSString *)name atIndex:(NSInteger)index andSection:(NSInteger)section
{
    NSLog(@"popover result: %@ %li", name, (long)section);
    switch (section) {
        case 0:
        {
            self.affinity = name;
            if (self.qrResult) {
                self.url = [self.qrResult stringByReplacingOccurrencesOfString:@"Consumer" withString:self.affinity];
                [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.url]]];
            }
        }
            break;
        case 1:
            self.scanMode = name;
            break;
    }

}
#endif

int nextIndex = -1;
bool securityBannerWasHidden = NO;

- (IBAction)hintPressed:(id)sender
{
    nextIndex = 1;
    
    [self configureHint];
}

-(void)configureHint
{
    HintViewController *hvc = [self.storyboard instantiateViewControllerWithIdentifier:@"HintPopup"];
    hvc.modalPresentationStyle = UIModalPresentationPopover;
    hvc.preferredContentSize = CGSizeMake(300, 140);
    hvc.view.backgroundColor = [UIColor colorFromHexString:@"009ed2"];
    hvc.popoverPresentationController.delegate = self;
    hvc.popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionUp;
    hvc.index = nextIndex;
    hvc.handler = self;

    switch (nextIndex) {
        case 1:
            hvc.popoverPresentationController.sourceView = self.backButton;
            hvc.popoverPresentationController.sourceRect = self.backButton.bounds;
            hvc.label.text = @"Click here to go to the previous page";
            break;
        case 2:
            hvc.popoverPresentationController.sourceView = self.securityBackground;
            hvc.popoverPresentationController.sourceRect = self.securityBackground.bounds;
            hvc.label.text = @"This bar will show any security risks for your recent scan";
            if (self.securityBackground.isHidden) {
                [self showSecurityBannerOK:YES withHidden:NO fade:NO];
                securityBannerWasHidden = YES;
            }
            break;
        case 3:
        {
//            UIView *view = [[self.tabBar.items objectAtIndex:0] valueForKey:@"view"];
            hvc.popoverPresentationController.sourceView = self.tabBar;
            hvc.popoverPresentationController.sourceRect = self.tabBar.bounds;
            hvc.label.text = @"Click here to go to Scan a QR Code, see your recent Scans, or adjust your Settings";
            hvc.nextButton.hidden = YES;
        }
            break;
#ifdef NotNow
        case 4:
        {
            UIView *view = [[self.tabBar.items objectAtIndex:1] valueForKey:@"view"];
            hvc.popoverPresentationController.sourceView = view;
            hvc.popoverPresentationController.sourceRect = view.bounds;
            hvc.label.text = @"Click here to see your recent Scan History";
        }
            break;
        case 5:
        {
            UIView *view = [[self.tabBar.items objectAtIndex:2] valueForKey:@"view"];
            hvc.popoverPresentationController.sourceView = view;
            hvc.popoverPresentationController.sourceRect = view.bounds;
            hvc.label.text = @"Click here to adjust your Settings";
            hvc.nextButton.hidden = YES;
        }
            break;
#endif
    }

    [self presentViewController:hvc animated:YES completion:nil];
}

- (void)hintChanged:(int)select
{
    if (securityBannerWasHidden) {
        [self showSecurityBannerOK:YES withHidden:YES fade:NO];
    }
    nextIndex = select;
    if (nextIndex > 0)
        [self configureHint];
}

- (void)popoverPresentationControllerDidDismissPopover:(UIPopoverPresentationController *)popoverPresentationController
{
    if (securityBannerWasHidden) {
        [self showSecurityBannerOK:YES withHidden:YES fade:NO];
    }
}

- (UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller
{
    return UIModalPresentationNone;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([@"QRScanner" isEqualToString:segue.identifier]) {
        QRScannerViewController *qrvc = segue.destinationViewController;
        qrvc.handler = self;
#ifdef NotNow
    } else if ([@"AffinityPopover" isEqualToString:segue.identifier]) {
        SimpleListViewController *affinityPopover = segue.destinationViewController;
        int iSize = (int)(self.affinities.count + self.scanModes.count + 2) * [SimpleListViewController rowHeight];
        if (iSize > 400)
            iSize = 400;
        affinityPopover.preferredContentSize = CGSizeMake(200, iSize);
        affinityPopover.affinities = self.affinities;
        affinityPopover.scanModes = self.scanModes;
        affinityPopover.affinity = self.affinity;
        affinityPopover.scanMode = self.scanMode;
        affinityPopover.handler = self;
        
        // configure the Popover presentation controller
        UIPopoverPresentationController *popoverController = [affinityPopover popoverPresentationController];
        popoverController.permittedArrowDirections = UIPopoverArrowDirectionUp;
        popoverController.sourceView = self.settingsButton;
        popoverController.sourceRect = self.settingsButton.bounds;
        popoverController.delegate = self;
#endif
    }
}

@end
