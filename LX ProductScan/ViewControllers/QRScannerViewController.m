//
//  QRScannerViewController.m
//  SmartLabelDemo
//
//  Created by Jeff Kase on 9/5/17.
//  Copyright © 2017 LocatorX. All rights reserved.
//

@import AVFoundation;

#import "QRScannerViewController.h"
#import "UIViewController+AppDelegate.h"

@interface QRScannerViewController () <AVCaptureMetadataOutputObjectsDelegate>

@property (weak, nonatomic) IBOutlet UIView *topBar;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalChildAssets;
@property (weak, nonatomic) IBOutlet UIView *totalChildAssetsBackground;
@property (weak, nonatomic) IBOutlet UIButton *countdownButton;

@property (nonatomic) AVCaptureDevice            *defaultDevice;
@property (nonatomic) AVCaptureDeviceInput       *defaultDeviceInput;
@property (nonatomic) AVCaptureDevice            *frontDevice;
@property (nonatomic) AVCaptureDeviceInput       *frontDeviceInput;
@property (nonatomic) AVCaptureMetadataOutput    *metadataOutput;
@property (nonatomic) AVCaptureSession           *captureSession;
@property (nonatomic) AVCaptureVideoPreviewLayer *previewLayer;

@property int timerTicks;

@end

@implementation QRScannerViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:@"Raleway-Bold" size:21]}];

    self.defaultDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    self.countdownButton.backgroundColor = self.appDelegate.buttonColor;
//    self.totalChildAssets.textColor = self.appDelegate
    self.messageLabel.textColor = UIColor.blackColor;
    self.messageLabel.backgroundColor = self.appDelegate.text1Color;

    NSError *error;
    self.defaultDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:self.defaultDevice error:&error];
    self.metadataOutput     = [[AVCaptureMetadataOutput alloc] init];
    [self.metadataOutput setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
    self.captureSession     = [[AVCaptureSession alloc] init];
    
    [self.captureSession addInput:self.defaultDeviceInput];
    [self.captureSession addOutput:self.metadataOutput];
    
    self.metadataOutput.metadataObjectTypes = @[AVMetadataObjectTypeQRCode];

    self.previewLayer       = [AVCaptureVideoPreviewLayer layerWithSession:self.captureSession];
    [self.previewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.view.layer addSublayer:self.previewLayer];
    self.previewLayer.frame = self.view.layer.bounds;
    
    [self.view bringSubviewToFront:self.topBar];
    [self.view bringSubviewToFront:self.messageLabel];
    
    [self.view bringSubviewToFront:self.totalChildAssetsBackground];
    [self.view bringSubviewToFront:self.totalChildAssets];
    
    self.totalChildAssetsBackground.hidden = self.sourceTag == -1;
    self.totalChildAssets.hidden = self.sourceTag == -1;
    self.countdownButton.hidden = YES;

    [self.captureSession startRunning];
}

- (IBAction)backButton:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (IBAction)closeButtonPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
//    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - AVCaptureMetadataOutputObjects Delegate Methods

- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection
{
    for (AVMetadataObject *current in metadataObjects) {
        if ([current isKindOfClass:[AVMetadataMachineReadableCodeObject class]]) {
            NSString *scannedResult = [(AVMetadataMachineReadableCodeObject *)current stringValue];
//            NSLog(@"result: %@", scannedResult);
//            self.messageLabel.text = scannedResult;
            NSLog(@"found QR");
            [self.captureSession stopRunning];
            
            if (self.handler && [self.handler respondsToSelector:@selector(found:withSourceTag:viewController:andBlock:)]) {
                NSLog(@"handler ok sending found");
                [self.handler found:scannedResult withSourceTag:self.sourceTag viewController:self andBlock:^(bool rescan) {
                    if (rescan) {
                        NSLog(@"resuming from QR");
                        [self.captureSession startRunning];
                    } else {
                        NSLog(@"closing nav from QR");
                        [self.navigationController popViewControllerAnimated:NO];
                    }
                }];
            }
            
            break;
        }
    }
}

@end
