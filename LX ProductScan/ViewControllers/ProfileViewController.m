//
//  ProfileViewController.m
//  LX Logistics Pro
//
//  Created by Jeff Kase on 8/31/20.
//  Copyright © 2020 Jeff Kase. All rights reserved.
//

#import "ProfileViewController.h"
#import "Lockbox.h"
#import "UIViewController+AppDelegate.h"

@interface ProfileViewController ()

@property (weak, nonatomic) IBOutlet UISwitch *soundSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *vibrationSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *saveHistorySwitch;

@end

@implementation ProfileViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSString *sound = [Lockbox stringForKey:@"sound"];
    self.soundSwitch.on = [@"true" isEqualToString:sound];
    self.soundSwitch.onTintColor = self.appDelegate.buttonColor;
    self.soundSwitch.tintColor = self.appDelegate.buttonColor;
    NSString *vibrate = [Lockbox stringForKey:@"vibrate"];
    self.vibrationSwitch.on = [@"true" isEqualToString:vibrate];
    self.vibrationSwitch.onTintColor = self.appDelegate.buttonColor;
    self.vibrationSwitch.tintColor = self.appDelegate.buttonColor;
    NSString *saveHistory = [Lockbox stringForKey:@"history"];
    self.saveHistorySwitch.on = [@"true" isEqualToString:saveHistory];
    self.saveHistorySwitch.onTintColor = self.appDelegate.buttonColor;
    self.saveHistorySwitch.tintColor = self.appDelegate.buttonColor;
}

- (IBAction)eventHistoryPressed:(id)sender
{
    if (self.handler && [self.handler respondsToSelector:@selector(doEventHistory)]) {
        [self.handler doEventHistory];
    }
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)soundPressed:(UISwitch *)button
{
    [Lockbox setString:(button.on ? @"true" : @"false") forKey:@"sound"];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)vibrationPressed:(UISwitch *)button
{
    [Lockbox setString:(button.on ? @"true" : @"false") forKey:@"vibrate"];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)saveHistoryPressed:(UISwitch *)button
{
    [Lockbox setString:(button.on ? @"true" : @"false") forKey:@"history"];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)faqPressed:(id)sender
{
    if (self.handler && [self.handler respondsToSelector:@selector(doFAQ)]) {
        [self.handler doFAQ];
    }
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)privacyPolicyPressed:(id)sender
{
    if (self.handler && [self.handler respondsToSelector:@selector(doPrivacyPolicy)]) {
        [self.handler doPrivacyPolicy];
    }
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)termsPressed:(id)sender
{
    if (self.handler && [self.handler respondsToSelector:@selector(doTerms)]) {
        [self.handler doTerms];
    }
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)locatorXPressed:(id)sender
{
    if (self.handler && [self.handler respondsToSelector:@selector(doLocatorX)]) {
        [self.handler doLocatorX];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
