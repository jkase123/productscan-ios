//
//  HintViewController.h
//  LX ProductScan
//
//  Created by Jeff Kase on 3/5/20.
//  Copyright © 2020 Jeff Kase. All rights reserved.
//

@import UIKit;

@protocol HintChange

@required
-(void)hintChanged:(int)select;

@end

@interface HintViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;

@property int index;
@property (nonatomic) id<HintChange> handler;

@end
