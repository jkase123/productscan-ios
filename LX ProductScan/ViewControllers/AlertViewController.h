//
//  AlertViewController.h
//  ProductScan
//
//  Created by Jeff Kase on 6/19/19.
//  Copyright © 2019 Jeff Kase. All rights reserved.
//

@import UIKit;

NS_ASSUME_NONNULL_BEGIN

@interface AlertViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *label;

@end

NS_ASSUME_NONNULL_END
