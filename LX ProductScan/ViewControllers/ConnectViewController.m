//
//  ConnectViewController.m
//  LX ProductScan
//
//  Created by Jeff Kase on 3/10/20.
//  Copyright © 2020 Jeff Kase. All rights reserved.
//

@import WebKit;
@import AudioToolbox;

#import "ConnectViewController.h"
#import "UIViewController+AppDelegate.h"
#import "AppDelegate+Location.h"
#import "AppDelegate+UrlSession.h"
#import "AppDelegate+Data.h"
#import "NSMutableArray+Assets.h"
#import "NSMutableDictionary+Asset.h"
#import "NSCombined+Values.h"
#import "SimpleListViewController.h"
#import "UIColor+Convert.h"
#import "UIViewController+Alert.h"
#import "AlertViewController.h"
#import "Lockbox.h"
//#import "UIViewController+BackButtonHandler.h"

@interface ConnectViewController () <WKNavigationDelegate, WKUIDelegate>

@property (weak, nonatomic) IBOutlet WKWebView *webView;
@property (weak, nonatomic) IBOutlet UIView *securityBackground;
@property (weak, nonatomic) IBOutlet UIImageView *securityImage;
@property (weak, nonatomic) IBOutlet UILabel *securityText;
@property (nonatomic) NSString *url;

@end

@implementation ConnectViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
        
    self.webView.navigationDelegate = self;
    self.webView.UIDelegate = self;
    
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlack];
    if (!self.navigationItem.rightBarButtonItem.image)
        [self.navigationItem.rightBarButtonItem setImage:[UIImage imageNamed:@"icon_profile"]];
}

-(void)viewWillAppear:(BOOL)animated
{
    NSLog(@"showing connect view");
    [super viewWillAppear:animated];
    
    self.securityBackground.hidden = YES;
    [self.securityBackground setAlpha:0.0f];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.title = self.json[@"asset"][@"tag"];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:@"Raleway-Bold" size:21]}];
//    if (@available(iOS 13, *)) {
//        NSLog(@"got 13");
//    } else {
//        NSLog(@"prior to 13");
//        [self.navigationItem.rightBarButtonItem setImage:[UIImage imageNamed:@"icon_profile"]];
//    }

//    bool valid =
    NSSet *websiteDataTypes = [NSSet setWithArray:@[
                                                    WKWebsiteDataTypeDiskCache,
                                                    WKWebsiteDataTypeOfflineWebApplicationCache,
                                                    WKWebsiteDataTypeMemoryCache,
                                                    //WKWebsiteDataTypeLocalStorage,
                                                    WKWebsiteDataTypeCookies,
                                                    //WKWebsiteDataTypeSessionStorage,
                                                    //WKWebsiteDataTypeIndexedDBDatabases,
                                                    //WKWebsiteDataTypeWebSQLDatabases,
                                                    WKWebsiteDataTypeFetchCache, //(iOS 11.3, *)
                                                    //WKWebsiteDataTypeServiceWorkerRegistrations, //(iOS 11.3, *)
                                                    ]];
    NSDate *dateFrom = [NSDate dateWithTimeIntervalSince1970:0];
    [[WKWebsiteDataStore defaultDataStore] removeDataOfTypes:websiteDataTypes modifiedSince:dateFrom completionHandler:^{
    }];

    NSString *url = self.json[@"url"];
    NSLog(@"url: %@", url);
    
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
    
    [self showSecurityElements:[self.json[@"isCounterfeit"] safeBoolValue]
                                  missingQR:[self.json[@"missingQR"] safeBoolValue]
                                 missingNFC:[self.json[@"missingNFC"] safeBoolValue]
                                   nfcValid:[self.json[@"nfcValid"] safeBoolValue]
                                    flagged:[self.json[@"flagged"] safeBoolValue]];
}

- (IBAction)backPressed:(id)sender
{
    if (self.webView.canGoBack)
        [self.webView goBack];
    else
        [self.navigationController popViewControllerAnimated:YES];
}

-(void)showSecurityBannerOK:(bool)ok withHidden:(bool)hidden fade:(bool)fade
{
    self.securityBackground.hidden = YES;
    [self.securityBackground setAlpha:0.0f];
    if (hidden)
        return;
    
    self.securityBackground.hidden = NO;
    [self.view bringSubviewToFront:self.securityBackground];
    if (ok) {
        self.securityBackground.backgroundColor = [UIColor colorFromHexString:@"a0c46d"];
        self.securityText.text = @"Valid";
        self.securityImage.image = [UIImage imageNamed:@"white_check"];
    } else {
        self.securityBackground.backgroundColor = [UIColor colorFromHexString:@"eb4f35"];
        self.securityText.text = @"WARNING";
        self.securityImage.image = [UIImage imageNamed:@"white_x"];
    }
    if (fade) {
        [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
            // fade in
            [self.securityBackground setAlpha:1.0f];
        } completion:^(BOOL finished) {
            //fade out
            [UIView animateWithDuration:1.0 delay:2.5 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
                [self.securityBackground setAlpha:0.0f];
            } completion:nil];
        }];
    } else {
        [self.securityBackground setAlpha:1.0f];
    }
}

-(bool)showSecurityElements:(BOOL)isCounterfeit missingQR:(BOOL)missingQR missingNFC:(BOOL)missingNFC nfcValid:(BOOL)nfcValid flagged:(BOOL)flagged
{
//    BOOL showAlert = NO;
//    UIColor *alertBackgroundColor;
//    NSString *alertText;
    bool valid = YES;
    
    if (isCounterfeit || flagged) {
        [self showSecurityBannerOK:NO withHidden:NO fade:YES];
        valid = NO;
    } else
        [self showSecurityBannerOK:YES withHidden:NO fade:YES];
    
#ifdef NotNow
    if (showAlert) {
        AlertViewController *avc = [self.storyboard instantiateViewControllerWithIdentifier:@"AlertPopup"];
        avc.modalPresentationStyle = UIModalPresentationPopover;
        avc.preferredContentSize = CGSizeMake(300, 80);
        avc.view.backgroundColor = alertBackgroundColor;
        avc.popoverPresentationController.delegate = self;
        avc.popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionUp;
        avc.popoverPresentationController.sourceView = self.backButton;
        avc.popoverPresentationController.sourceRect = self.backButton.bounds;
        avc.label.text = alertText;
        
        [self presentViewController:avc animated:YES completion:nil];
        
//        AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
    }
#endif
    return valid;
}

- (void)webView:(WKWebView *)webView runJavaScriptTextInputPanelWithPrompt:(NSString *)prompt defaultText:(NSString *)defaultText initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(NSString *result))completionHandler
{
    NSString *result = @"done";
    if ([prompt hasPrefix:@"scrollEnable"]) {
        webView.scrollView.scrollEnabled = YES;
    } else if ([prompt hasPrefix:@"scrollDisable"]) {
        webView.scrollView.scrollEnabled = NO;
    } else if ([prompt hasPrefix:@"enableBack"]) {
        NSLog(@"prompt");
//        webView.scrollView.scrollEnabled = YES;
    } else if ([prompt hasPrefix:@"disableBack"]) {
        NSLog(@"disableBack");
//        webView.scrollView.scrollEnabled = NO;
    } else if ([prompt hasPrefix:@"close"]) {
        [self.navigationController popViewControllerAnimated:YES];
    } else if ([prompt hasPrefix:@"openUrl "]) {
        NSArray *items = [prompt componentsSeparatedByString:@" "];
        if (items.count == 2) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:items[1]] options:@{} completionHandler:nil];
        }
    } else if ([prompt hasPrefix:@"get="]) {
        NSArray *items = [prompt componentsSeparatedByString:@"="];
        if (items.count == 2) {
            result = [Lockbox stringForKey:items[1]];
        }
    } else if ([prompt hasPrefix:@"set="]) {
        NSArray *items = [prompt componentsSeparatedByString:@"="];
        if (items.count == 3) {
            result = [Lockbox stringForKey:items[2]];
            [Lockbox setString:items[2] forKey:items[1]];
        }
    } else if ([prompt hasPrefix:@"toggle="]) {
        NSArray *items = [prompt componentsSeparatedByString:@"="];
        if (items.count == 2) {
            result = [Lockbox stringForKey:items[1]];
            if (!result || [result isEqualToString:@"false"]) {
                result = @"true";
                [Lockbox setString:@"true" forKey:items[1]];
            } else {
                result = @"false";
                [Lockbox setString:@"false" forKey:items[1]];
            }
        }
    }
    completionHandler(result);
}

- (IBAction)navbarBackButton:(id)sender
{
    NSLog(@"nav should pop");
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation
{
//    self.hasBack = webView.canGoBack;
}

/*
- (void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation
{
    NSLog(@"***** did commit nav");
}

- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation
{
    NSLog(@"***** did start prov");
}
*/

- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error
{
    NSLog(@"***** failed nav *****");
}

@end
