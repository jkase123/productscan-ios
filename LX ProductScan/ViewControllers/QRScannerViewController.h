//
//  QRScannerViewController.h
//  SmartLabelDemo
//
//  Created by Jeff Kase on 9/5/17.
//  Copyright © 2017 LocatorX. All rights reserved.
//

@import UIKit;

@protocol QRScannerResult <NSObject>

@optional
-(void)found:(NSString *)result withSourceTag:(NSInteger)tag viewController:(UIViewController *)viewController andBlock:(void(^)(bool rescan)) block;

@end

@interface QRScannerViewController : UIViewController

@property (nonatomic) id<QRScannerResult> handler;
@property NSInteger sourceTag;

@end
