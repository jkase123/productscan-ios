//
//  ScanViewController.h
//  LX ProductScan
//
//  Created by Jeff Kase on 3/10/20.
//  Copyright © 2020 Jeff Kase. All rights reserved.
//

@import UIKit;

#import "ContentViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ScanViewController : ContentViewController

@end

NS_ASSUME_NONNULL_END
