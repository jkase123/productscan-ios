//
//  SimpleListViewController.m
//  RestorationLibrary
//
//  Created by Jeff Kase on 1/8/15.
//  Copyright (c) 2015 Jeff Kase. All rights reserved.
//

#import "SimpleListViewController.h"

@interface SimpleListViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UIImageView *greenCheck;

@end

@implementation SimpleListViewCell

-(void)setData:(NSString *)name selected:(NSString *)selected
{
    self.title.text = name;
    self.greenCheck.hidden = ![name isEqualToString:selected];
}

@end

@interface SectionHeaderViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *title;

@end

@implementation SectionHeaderViewCell

-(void)setData:(NSString *)name
{
    self.title.text = name;
}

@end

@interface SimpleListViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation SimpleListViewController

+(int)rowHeight
{
    return 40;
}

#pragma mark - Table view data source

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    SectionHeaderViewCell *header = [tableView dequeueReusableCellWithIdentifier:@"SectionHeader"];
    header.title.text = (section == 0 ? @"Affinity" : @"Scan Mode");
    return header;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [SimpleListViewController rowHeight];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return [self.affinities count];
        case 1:
            return [self.scanModes count];
    }
    return 0;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *selection = nil;
    
    switch (indexPath.section) {
        case 0:
            selection = self.affinities[indexPath.row];
            break;
        case 1:
            selection = self.scanModes[indexPath.row];
            break;
    }

    if (self.handler && [self.handler respondsToSelector:@selector(simpleListDidSelect:atIndex:andSection:)]) {
        [self.handler simpleListDidSelect:selection atIndex:indexPath.row andSection:indexPath.section];
    }
    
    if (self.m_block)
        self.m_block(selection, indexPath.row, indexPath.section);

    [self dismissViewControllerAnimated:YES completion:nil];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SimpleListViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SimpleListViewCell" forIndexPath:indexPath];
    
    switch (indexPath.section) {
        case 0:
            [cell setData:self.affinities[indexPath.row] selected:self.affinity];
            break;
        case 1:
            [cell setData:self.scanModes[indexPath.row] selected:self.scanMode];
            break;
    }
    
    return cell;
}

@end
