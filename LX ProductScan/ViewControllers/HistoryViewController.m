//
//  HistoryViewController.m
//  LX ProductScan
//
//  Created by Jeff Kase on 3/10/20.
//  Copyright © 2020 Jeff Kase. All rights reserved.
//

#import "HistoryViewController.h"
#import "UIViewController+AppDelegate.h"
#import "AppDelegate+Data.h"
#import "NSMutableArray+Assets.h"
#import "NSMutableDictionary+Asset.h"
#import "NSCombined+Values.h"
#import "UIColor+Convert.h"
#import "TableFooterView.h"
#include "WebViewController.h"

@interface EventActionCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *timeOfLog;
@property (weak, nonatomic) IBOutlet UILabel *assetTag;

@property (nonatomic) NSMutableDictionary *asset;

@end

@implementation EventActionCell

-(void)setData:(NSMutableDictionary *)asset withColor:(UIColor *)color
{
    self.asset = asset;
    
    self.timeOfLog.text = asset.timeOfVisitString;
    self.assetTag.text = asset.tag;
    
    self.assetTag.textColor = color;
}

@end

@interface HistoryViewController () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic) NSMutableArray *assetList;

@end

@implementation HistoryViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.assetList = [self.appDelegate.assets getAssetsByTime];
    
    self.tableView.backgroundColor = [UIColor whiteColor];
//    self.tableView.backgroundColor = [UIColor colorFromHexString:@"32355C"];
    self.tableView.tableFooterView = [[TableFooterView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 30)];
}

- (IBAction)backButton:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:@"Raleway-Bold" size:21]}];
    
    [self.tableView reloadData];
}

#pragma mark - UITableViewDataSource

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    NSMutableDictionary *eventDict = self.assetList[indexPath.row];
    EventActionCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EventAction"];
    [cell setData:eventDict withColor:self.appDelegate.text2Color];
    
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger numberOfRows = self.assetList.count;
    [(TableFooterView *)self.tableView.tableFooterView setHasData:(numberOfRows > 0) alternateMessage:@"no assets available"];
    return numberOfRows;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 66;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([@"AssetWeb" isEqualToString:segue.identifier]) {
        WebViewController *wvc = segue.destinationViewController;
        EventActionCell *cell = (EventActionCell *)sender;

        wvc.url = cell.asset.url;
        wvc.webTitle = cell.asset.tag;
    }

}

@end
