//
//  SimpleListViewController.h
//  RestorationLibrary
//
//  Created by Jeff Kase on 1/8/15.
//  Copyright (c) 2015 Jeff Kase. All rights reserved.
//

@import UIKit;

@protocol SimpleListHandler <NSObject>

@optional
-(void)simpleListDidCancel;
-(void)simpleListDidSelect:(NSString *)name atIndex:(NSInteger)index andSection:(NSInteger)section;

@end

@interface SimpleListViewController : UIViewController

@property (nonatomic) id<SimpleListHandler> handler;
@property (copy) void (^m_block)(NSString *name, NSInteger index, NSInteger section);
@property (nonatomic) NSArray *affinities, *scanModes;
@property (nonatomic) NSString *affinity, *scanMode;

+(int)rowHeight;

@end
