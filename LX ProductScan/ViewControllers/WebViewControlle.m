//
//  WebViewController.m
//  LX ProductScan
//
//  Created by Jeff Kase on 3/10/20.
//  Copyright © 2020 Jeff Kase. All rights reserved.
//

@import WebKit;

#import "WebViewController.h"
#import "UIViewController+AppDelegate.h"
#import "UIColor+Convert.h"
#import "Lockbox.h"

@interface WebViewController () <WKNavigationDelegate, WKUIDelegate>

@property (weak, nonatomic) IBOutlet WKWebView *webView;

@end

@implementation WebViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
        
    self.webView.navigationDelegate = self;
    self.webView.UIDelegate = self;
}

- (IBAction)backButton:(id)sender
{
    if (self.webView.canGoBack)
        [self.webView goBack];
    else
        [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.title = self.webTitle;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:@"Raleway-Bold" size:21]}];

//    bool valid =
    NSSet *websiteDataTypes = [NSSet setWithArray:@[
                                                    WKWebsiteDataTypeDiskCache,
                                                    WKWebsiteDataTypeOfflineWebApplicationCache,
                                                    WKWebsiteDataTypeMemoryCache,
                                                    //WKWebsiteDataTypeLocalStorage,
                                                    WKWebsiteDataTypeCookies,
                                                    //WKWebsiteDataTypeSessionStorage,
                                                    //WKWebsiteDataTypeIndexedDBDatabases,
                                                    //WKWebsiteDataTypeWebSQLDatabases,
                                                    WKWebsiteDataTypeFetchCache, //(iOS 11.3, *)
                                                    //WKWebsiteDataTypeServiceWorkerRegistrations, //(iOS 11.3, *)
                                                    ]];
    NSDate *dateFrom = [NSDate dateWithTimeIntervalSince1970:0];
    [[WKWebsiteDataStore defaultDataStore] removeDataOfTypes:websiteDataTypes modifiedSince:dateFrom completionHandler:^{
    }];

    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.url]]];
}

- (void)webView:(WKWebView *)webView runJavaScriptTextInputPanelWithPrompt:(NSString *)prompt defaultText:(NSString *)defaultText initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(NSString *result))completionHandler
{
    NSString *result = @"done";
    if ([prompt hasPrefix:@"scrollEnable"]) {
        webView.scrollView.scrollEnabled = YES;
    } else if ([prompt hasPrefix:@"scrollDisable"]) {
        webView.scrollView.scrollEnabled = NO;
    } else if ([prompt hasPrefix:@"enableBack"]) {
        NSLog(@"prompt");
//        webView.scrollView.scrollEnabled = YES;
    } else if ([prompt hasPrefix:@"disableBack"]) {
        NSLog(@"disableBack");
//        webView.scrollView.scrollEnabled = NO;
    } else if ([prompt hasPrefix:@"close"]) {
        [self.navigationController popViewControllerAnimated:YES];
    } else if ([prompt hasPrefix:@"openUrl "]) {
        NSArray *items = [prompt componentsSeparatedByString:@" "];
        if (items.count == 2) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:items[1]] options:@{} completionHandler:nil];
        }
    } else if ([prompt hasPrefix:@"get="]) {
        NSArray *items = [prompt componentsSeparatedByString:@"="];
        if (items.count == 2) {
            result = [Lockbox stringForKey:items[1]];
        }
    } else if ([prompt hasPrefix:@"set="]) {
        NSArray *items = [prompt componentsSeparatedByString:@"="];
        if (items.count == 3) {
            result = [Lockbox stringForKey:items[2]];
            [Lockbox setString:items[2] forKey:items[1]];
        }
    } else if ([prompt hasPrefix:@"toggle="]) {
        NSArray *items = [prompt componentsSeparatedByString:@"="];
        if (items.count == 2) {
            result = [Lockbox stringForKey:items[1]];
            if (!result || [result isEqualToString:@"false"]) {
                result = @"true";
                [Lockbox setString:@"true" forKey:items[1]];
            } else {
                result = @"false";
                [Lockbox setString:@"false" forKey:items[1]];
            }
        }
    }
    completionHandler(result);
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation
{
//    self.homeViewController.backButton.enabled = webView.canGoBack;
}

/*
- (void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation
{
    NSLog(@"***** did commit nav");
}

- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation
{
    NSLog(@"***** did start prov");
}
*/

- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error
{
    NSLog(@"***** failed nav *****");
}

@end
