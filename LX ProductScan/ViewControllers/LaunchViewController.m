//
//  LaunchViewController.m
//  LX ProductScan
//
//  Created by Jeff Kase on 3/4/20.
//  Copyright © 2020 Jeff Kase. All rights reserved.
//

#import "LaunchViewController.h"
#import "UIViewController+AppDelegate.h"
#import "AppDelegate+UrlSession.h"
#import "AppDelegate+Data.h"
#import "NSMutableDictionary+AppSetting.h"

@interface LaunchViewController ()

@property (weak, nonatomic) IBOutlet UIButton *getStartedButton;
@property (weak, nonatomic) IBOutlet UIButton *termsOfUse;
@property (weak, nonatomic) IBOutlet UIButton *privacyPolicy;
@property (weak, nonatomic) IBOutlet UIView *verticalDivider;
@property (weak, nonatomic) IBOutlet UILabel *copyright;
@property (weak, nonatomic) IBOutlet UILabel *psTitle;
@property (weak, nonatomic) IBOutlet UILabel *welcome;
@property (weak, nonatomic) IBOutlet UILabel *paragraph1;
@property (weak, nonatomic) IBOutlet UILabel *paragraph2;
@property (weak, nonatomic) IBOutlet UIImageView *largeIcon;

@property bool hasCompletedAnimation, hasStartupData;

@end

@implementation LaunchViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.hasCompletedAnimation = !self.appDelegate.appSettings.seenGetStarted;
    self.hasStartupData = !self.appDelegate.appSettings.seenGetStarted;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self configureUI:self.appDelegate.appSettings.seenGetStarted];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(settingsChanged:) name:kAppSettingsChange object:nil];

    if (self.appDelegate.connectHome && self.hasCompletedAnimation)
        [self.appDelegate setRootViewController:[[UIStoryboard storyboardWithName:@"Home" bundle:nil] instantiateInitialViewController]];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (self.appDelegate.appSettings.seenGetStarted) {
        float scale = self.view.frame.size.width / self.largeIcon.frame.size.width;
        [UIView animateWithDuration:2.0 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            self.largeIcon.center = self.view.center;
            self.largeIcon.transform = CGAffineTransformMakeScale(scale, scale);
        } completion:^(BOOL finished) {
            self.hasCompletedAnimation = YES;
            if (self.appDelegate.appSettings.seenGetStarted && self.hasStartupData) {
                    [self.appDelegate setRootViewController:[[UIStoryboard storyboardWithName:@"Home" bundle:nil] instantiateInitialViewController]];
            }
        }];
    }
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)configureUI:(bool)hideGetStarted
{
    self.getStartedButton.hidden = hideGetStarted;
    self.psTitle.hidden = hideGetStarted;
    self.welcome.hidden = hideGetStarted;
    self.paragraph1.hidden = hideGetStarted;
    self.paragraph2.hidden = hideGetStarted;
    self.verticalDivider.hidden = hideGetStarted;
    self.termsOfUse.hidden = hideGetStarted;
    self.privacyPolicy.hidden = hideGetStarted;
    self.copyright.hidden = hideGetStarted;
}

-(void)settingsChanged:(NSNotification *)note
{
    dispatch_sync(dispatch_get_main_queue(), ^{
        self.hasStartupData = YES;
        if (self.appDelegate.appSettings.seenGetStarted && self.hasCompletedAnimation) {
                [self.appDelegate setRootViewController:[[UIStoryboard storyboardWithName:@"Home" bundle:nil] instantiateInitialViewController]];
        }
    });
}

- (IBAction)getStartedPressed:(id)sender
{
    self.appDelegate.appSettings.seenGetStarted = YES;
    NSMutableDictionary *d = self.appDelegate.m_data;
    [self.appDelegate saveAppSettingsData];
    
    [self.appDelegate setRootViewController:[[UIStoryboard storyboardWithName:@"Home" bundle:nil] instantiateInitialViewController]];
}

- (IBAction)privacyPolicyPressed:(UIButton *)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.appDelegate.privacyPolicy] options:@{} completionHandler:nil];
}

- (IBAction)termsOfUsePressed:(UIButton *)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.appDelegate.termsOfUse] options:@{} completionHandler:nil];
}

@end
