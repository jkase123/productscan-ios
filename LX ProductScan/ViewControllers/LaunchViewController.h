//
//  LaunchViewController.h
//  LX ProductScan
//
//  Created by Jeff Kase on 3/4/20.
//  Copyright © 2020 Jeff Kase. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LaunchViewController : UIViewController

@end

NS_ASSUME_NONNULL_END
