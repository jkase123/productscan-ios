//
//  HintViewController.m
//  LX ProductScan
//
//  Created by Jeff Kase on 3/5/20.
//  Copyright © 2020 Jeff Kase. All rights reserved.
//

#import "HintViewController.h"

@interface HintViewController ()

@end

@implementation HintViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (IBAction)doneButtonPressed:(id)sender
{
    if (self.handler)
        [self.handler hintChanged:-1];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)nextButtonPressed:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
        if (self.handler)
            [self.handler hintChanged:self.index + 1];
    }];
}

@end
