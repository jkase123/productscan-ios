//
//  ContentViewController.h
//  LX ProductScan
//
//  Created by Jeff Kase on 3/10/20.
//  Copyright © 2020 Jeff Kase. All rights reserved.
//

@import UIKit;

#import "ProfileViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ContentViewController : UIViewController <UIPopoverPresentationControllerDelegate, ProfileHandler>

@property (nonatomic, nullable) NSString *assetId, *scanMode, *qrHash;

-(bool)setAssetIdFromResult:(NSString *)url;
+(NSArray *)parseAssetIdHashFromResult:(NSString *)url;

@end

NS_ASSUME_NONNULL_END
