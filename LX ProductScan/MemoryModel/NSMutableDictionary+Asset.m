//
//  NSMutableDictionary+Action.m
//  RestorationTest
//
//  Created by Jeff Kase on 10/9/14.
//  Copyright (c) 2014 Jeff Kase. All rights reserved.
//

@import Foundation;

#import "NSMutableDictionary+Asset.h"
#import "NSCombined+Values.h"

@implementation NSMutableDictionary (Asset)

-(NSString *)assetId
{
    return [self[@"assetId"] safeStringValue];
}

-(void)setAssetId:(NSString *)assetId
{
    self[@"assetId"] = assetId;
}

-(NSString *)url
{
    return [self[@"url"] safeStringValue];
}

-(void)setUrl:(NSString *)url
{
    self[@"url"] = url;
}

-(NSString *)tag
{
    return [self[@"tag"] safeStringValue];
}

-(void)setTag:(NSString *)tag
{
    self[@"tag"] = tag;
}

-(NSString *)timeOfVisitString
{
    return [self[@"timeOfVisitString"] safeStringValue];
}

-(void)setTimeOfVisitString:(NSString *)timeOfVisitString
{
    self[@"timeOfVisitString"] = timeOfVisitString;
}

-(int)timeOfVisit
{
    return [self[@"timeOfVisit"] safeIntValue];
}

-(void)setTimeOfVisit:(int)timeOfVisit
{
    self[@"timeOfVisit"] = [NSNumber numberWithInt:timeOfVisit];
}

@end
