//
//  AppDelegate+Data.m
//

#import "AppDelegate+Data.h"
#import "NSMutableDictionary+Asset.h"
#import "NSCombined+Values.h"

@implementation AppDelegate (Data)

-(NSMutableArray *)assets
{
    return self.m_data[kAssetTag];
}

-(void)setAssets:(NSMutableArray *)assets
{
    self.m_data[kAssetTag] = assets;
}

-(void)saveAssetData
{
    [self saveData:kAssetTag];
}

-(NSMutableDictionary *)appSettings
{
    return self.m_data[kAppSettingsTag];
}

-(void)setAppSettings:(NSMutableDictionary *)appSettings
{
    self.m_data[kAppSettingsTag] = appSettings;
}

-(void)saveAppSettingsData
{
    [self saveData:kAppSettingsTag];
}

-(NSURL *)documentUrlForTag:(NSString *)tag
{
    return [[self applicationDocumentsDirectory] URLByAppendingPathComponent:[NSString stringWithFormat:@"%@.json", tag]];
#ifdef NotNow
    NSURL *userFolderUrl = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:[NSString stringWithFormat:@"User_%06i", self.appUser.appUserId]];
    [[NSFileManager defaultManager] createDirectoryAtURL:userFolderUrl withIntermediateDirectories:YES attributes:nil error:nil];
    return [userFolderUrl URLByAppendingPathComponent:[NSString stringWithFormat:@"%@.json", tag]];
//#else
    return nil;
#endif
}

-(void)saveData:(NSString *)tag
{
    NSURL *url = [self documentUrlForTag:tag];
    NSOutputStream *outStream = [NSOutputStream outputStreamWithURL:url append:NO];
    
    [outStream open];
    
#ifdef NotNow
    NSLog(@"data for %@: %@", tag, @{tag:self.m_data[tag]});
    NSError *error;
    NSData *testData = [NSJSONSerialization dataWithJSONObject:@{tag:self.m_data[tag]} options:0 error:&error];
    NSString *test = [[NSString alloc] initWithData:testData encoding:NSUTF8StringEncoding];
     
    NSInteger iCount = [NSJSONSerialization writeJSONObject:@{@"test":@"test"}
                                                   toStream:outStream
                                                    options:0
                                                      error:&error];
#endif
    
    NSInteger iCount = [NSJSONSerialization writeJSONObject:@{tag:self.m_data[tag]}
                                                   toStream:outStream
                                                    options:0
                                                      error:NULL];
    [outStream close];
    
    NSLog(@"write %li bytes", (long)iCount);
}

enum {
    kDataArray,
    kDataDictionary,
    kDataString,
    kDataNumber
};

-(void)loadData:(NSString *)tag type:(int)dataType
{
    NSURL *url = [self documentUrlForTag:tag];
//    NSLog(@"url %@", url.absoluteString);
    NSInputStream *inStream = [NSInputStream inputStreamWithURL:url];
    if (inStream) {
        
        [inStream open];
        NSDictionary *data = [NSJSONSerialization JSONObjectWithStream:inStream
                                                               options:NSJSONReadingMutableContainers error:nil];
        [inStream close];
        
        if (data && data[tag]) {
            self.m_data[tag] = data[tag];
            return;
        }
    }
    switch (dataType) {
        case kDataArray:
            self.m_data[tag] = [NSMutableArray array];
            break;
        case kDataDictionary:
            self.m_data[tag] = [NSMutableDictionary dictionary];
            break;
        case kDataString:
            self.m_data[tag] = [NSString string];
            break;
        case kDataNumber:
            self.m_data[tag] = @0;
            break;
    }
}

-(void)initializeData
{
    self.m_data = [NSMutableDictionary dictionaryWithCapacity:3];
    
    [self loadData:kAppSettingsTag type:kDataDictionary];
    [self loadData:kAssetTag type:kDataArray];

    NSDateFormatter *dfOut = [[NSDateFormatter alloc] init];
    [dfOut setDateFormat:[NSDateFormatter dateFormatFromTemplate:@"MMddyyyyhhmmssa" options:0 locale:NSLocale.systemLocale]];

//    NSInteger timeZoneSeconds = [[NSTimeZone localTimeZone] secondsFromGMT];

    for (NSMutableDictionary *asset in self.assets) {
        NSDate *gmtDate = [NSDate dateWithTimeIntervalSince1970:asset.timeOfVisit];
//        NSDate *localDate = [gmtDate dateByAddingTimeInterval:timeZoneSeconds];
        asset.timeOfVisitString = [dfOut stringFromDate:gmtDate];
    }
    [self saveAssetData];
}

@end
