//
//  AppDelegate+Data.h
//

@import Foundation;

#import "AppDelegate.h"

// data dictionary tags
#define kAssetTag           @"assets"
#define kAppSettingsTag     @"appSettings"

@interface AppDelegate (Data)

@property (nonatomic) NSMutableArray *assets;
@property (nonatomic) NSMutableDictionary *appSettings;

-(void)initializeData;

-(void)saveAssetData;
-(void)saveAppSettingsData;

@end
