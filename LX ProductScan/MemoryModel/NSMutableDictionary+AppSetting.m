//
//  NSMutableDictionary+AppSetting.m
//  RestorationTest
//
//  Created by Jeff Kase on 10/9/14.
//  Copyright (c) 2014 Jeff Kase. All rights reserved.
//

@import Foundation;

#import "NSMutableDictionary+AppSetting.h"
#import "NSCombined+Values.h"

@implementation NSMutableDictionary (AppSetting)

-(bool)seenGetStarted
{
    return [self[@"seenGetStarted"] safeBoolValue];
}

-(void)setSeenGetStarted:(bool)seenGetStarted
{
    self[@"seenGetStarted"] = [NSNumber numberWithBool:(bool)seenGetStarted];
}

@end
