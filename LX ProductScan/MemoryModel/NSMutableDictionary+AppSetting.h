//
//  NSMutableDictionary+AppSetting.h
//  RestorationTest
//
//  Created by Jeff Kase on 10/9/14.
//  Copyright (c) 2014 Jeff Kase. All rights reserved.
//

@import Foundation;
@import UIKit;

@interface NSMutableDictionary (AppSetting)

@property bool seenGetStarted;

@end
