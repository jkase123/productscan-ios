//
//  NSMutableDictionary+Action.h
//  RestorationTest
//
//  Created by Jeff Kase on 10/9/14.
//  Copyright (c) 2014 Jeff Kase. All rights reserved.
//

@import Foundation;
@import UIKit;

@interface NSMutableDictionary (Asset)

@property (nonatomic) NSString *assetId, *url, *tag, *timeOfVisitString;
@property int timeOfVisit;

@end
