//
//  NSMutableArray+Assets.m
//  RestorationTest
//
//  Created by Jeff Kase on 10/9/14.
//  Copyright (c) 2014 Jeff Kase. All rights reserved.
//

@import Foundation;

#import "NSMutableArray+Assets.h"
#import "NSCombined+Values.h"
#import "NSMutableDictionary+Asset.h"

@implementation NSMutableArray (Assets)

-(NSMutableDictionary *)newAsset
{
    NSMutableDictionary *asset = [NSMutableDictionary dictionary];
    
    [self addObject:asset];
    return asset;
}

-(void)addAsset:(NSMutableDictionary *)asset
{
    [self addObject:asset];
}

-(void)deleteAsset:(NSMutableDictionary *)asset
{
    [self removeObject:asset];
}

-(NSMutableDictionary *)getAssetForId:(NSString *)assetId
{
    NSArray *assets = [self filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"assetId = %@", assetId]];
    if (assets && [assets count] > 0) {
        return assets[0];
    }
    return nil;
}

-(NSMutableArray *)getAssetsByTime
{
    return [[self sortedArrayUsingDescriptors:@[[[NSSortDescriptor alloc] initWithKey:@"timeOfVisit" ascending:NO]]] mutableCopy];
}

#ifdef NotNow
#define kDeviceResidentMax  6

-(void)generateAssetsForList:(NSArray *)assetList
{
    NSLog(@"generateAssetsForList");

    NSMutableArray *downloadedVideos = [NSMutableArray arrayWithCapacity:self.count];
    
    for (NSMutableDictionary *rev in assetList) {
        int assetId = [rev[@"assetId"] safeIntValue];
        bool newAsset = NO;
        NSMutableDictionary *asset = [self getAssetForId:assetId];
        if (!asset) {
            asset = [self newAsset];
            asset.assetId = assetId;
            newAsset = YES;
            [downloadedVideos addObject:asset];
        } else {
            if (asset.hasLocalVideoForAsset)
                [downloadedVideos addObject:asset];
        }
        [asset setAssetFieldsFromDict:rev newAsset:(bool)newAsset];
        
//        NSMutableDictionary *asset = [self generateAssetForAssetId:[rev[@"assetId"] safeIntValue]];
//        [asset setAssetFieldsFromDict:rev newAsset:(bool)newAsset];
    }
    
    // now trim the list of resident files, ordered by time assigned
    //
    NSLog(@"downloadVideos count: %lu", (unsigned long)downloadedVideos.count);
    if (downloadedVideos.count > kDeviceResidentMax) {
        NSArray *triageList = [downloadedVideos sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"timeAssigned" ascending:NO]]];
        int iCount = 0;
        for (NSMutableDictionary *asset in triageList) {
            NSLog(@"checking %i: %@", iCount+1, asset);
            if (iCount++ < kDeviceResidentMax) {
                [asset checkLocalVideoForAssetFrom:@"assets"];
            } else {
                NSLog(@"  removing %@", asset.localVideoFilePathForAsset);
                [[NSFileManager defaultManager] removeItemAtPath:asset.localVideoFilePathForAsset error:nil];
            }
        }
    }
}

-(NSArray *)getAssetsInHierarchy
{
    NSMutableDictionary *statusDict = [NSMutableDictionary dictionary];
    for (NSMutableDictionary *asset in self) {
        NSNumber *stId = asset[@"statusId"];
        NSMutableDictionary *stDict = statusDict[stId];
        if (!stDict) {
            statusDict[stId] = stDict = [@{@"statusId":stId,@"status":asset.status,@"assets":[@[] mutableCopy]} mutableCopy];
        }
        [(NSMutableArray *)stDict[@"assets"] addObject:asset];
    }
    NSArray *assetArray = [[statusDict allValues] sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"statusId" ascending:YES]]];
    for (NSMutableDictionary *assetDict in assetArray) {
        [assetDict[@"assets"] sortUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"timeSubmitted" ascending:YES]]];
    }
    return assetArray;
}

-(NSArray *)getAssetsWithSortDescs:(NSArray *)sortDescs
{
    return [self sortedArrayUsingDescriptors:sortDescs];
}

#endif

@end
