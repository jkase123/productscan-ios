//
//  NSMutableArray+Assets.h
//

@interface NSMutableArray (Assets)

-(NSMutableDictionary *)newAsset;
-(void)addAsset:(NSMutableDictionary *)asset;
-(void)deleteAsset:(NSMutableDictionary *)asset;
-(NSMutableDictionary *)getAssetForId:(NSString *)assetId;
-(NSMutableArray *)getAssetsByTime;

@end
