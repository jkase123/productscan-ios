//
//  TableFooterView.h
//  LX Connect
//
//  Created by Jeff Kase on 4/12/20.
//  Copyright © 2020 Jeff Kase. All rights reserved.
//

@import UIKit;

NS_ASSUME_NONNULL_BEGIN

@interface TableFooterView : UIView

-(void)setHasData:(bool)hasData alternateMessage:(NSString *)message;

@end

NS_ASSUME_NONNULL_END
