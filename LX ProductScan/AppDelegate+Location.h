//
//  AppDelegate+Location.h
//  Locator
//
//  Created by Jeff Kase on 4/5/16.
//  Copyright © 2016 LocatorX. All rights reserved.
//

#import "AppDelegate.h"

#define kUpdatedLocation        @"updatedLocation"

@interface AppDelegate (Location) <CLLocationManagerDelegate>

-(void)initializeLocation;
-(void)checkAuthorization;
#ifndef firstRescueSim
-(void)startUpdatingLocationWithBlock:(void(^)(CLLocation *location)) block;
-(void)stopUpdatingLocation;
#endif
-(void)setActiveLocation:(BOOL)isActive;
-(void)getLocationWithBlock:(void(^)(CLLocation *location)) block;

@end
